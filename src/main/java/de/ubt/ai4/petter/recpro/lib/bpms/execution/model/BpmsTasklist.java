package de.ubt.ai4.petter.recpro.lib.bpms.execution.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsTasklist {
    private Long id;
    private String assignee;
    private List<BpmsTask> tasks;
}
