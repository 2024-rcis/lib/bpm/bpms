package de.ubt.ai4.petter.recpro.lib.bpms.util;

public final class BpmApiUrls {
    public static final String BASE_API_URL = "api/";
    public static final String BASE_MODELING_URL = BASE_API_URL + "modeling/";
    public static final String BASE_PROCESS_URL = BASE_MODELING_URL + "process/";
    public static final String PROCESS_GET_ALL = BASE_PROCESS_URL + "getAll";

    public static final String BASE_EXECUTION_URL = BASE_API_URL + "execution/";
    public static final String BASE_EXECUTION_TASKLIST_URL = BASE_EXECUTION_URL + "tasklist/";
    public static final String EXECUTION_TASKLIST_GET_BY_ASSIGNEE_URL = BASE_EXECUTION_TASKLIST_URL + "getByAssignee";
    public static final String EXECUTION_TASKLIST_GET_ALL = BASE_EXECUTION_TASKLIST_URL + "getAll";

    public static final String BASE_EXECUTION_TASK_URL = BASE_EXECUTION_URL + "task/";
    public static final String TASK_CLAIM = BASE_EXECUTION_TASK_URL + "claim/";
    public static final String TASK_UNCLAIM = BASE_EXECUTION_TASK_URL + "unclaim/";
    public static final String TASK_ASSIGN = BASE_EXECUTION_TASK_URL + "assign/";
    public static final String TASK_COMPLETE = BASE_EXECUTION_TASK_URL + "complete/";
    public static final String SET_TASK_ATTRIBUTE = BASE_EXECUTION_TASK_URL + "setAttribute/";
    public static final String SET_TASK_ATTRIBUTES = BASE_EXECUTION_TASK_URL + "setAttributes/";

    public static final String BASE_EXECUTION_PROCESS_URL = BASE_EXECUTION_URL + "process/";
    public static final String PROCESS_START = BASE_EXECUTION_PROCESS_URL + "start/";
    public static final String SET_PROCESS_ATTRIBUTE = BASE_EXECUTION_PROCESS_URL + "setAttribute/";
    public static final String SET_PROCESS_ATTRIBUTES = BASE_EXECUTION_PROCESS_URL + "setAttributes/";
    public static final String REQUEST_PARAM_ATTRIBUTE_ID = "&attributeId=";
    public static final String REQUEST_PARAM_ATTRIBUTE_VALUE = "&value=";
}
