package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.task;

import de.ubt.ai4.petter.recpro.lib.bpms.util.RequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;

import static de.ubt.ai4.petter.recpro.lib.bpms.util.BpmApiUrls.*;


@Profile("camunda")
@Service
@RequiredArgsConstructor
public class BpmsTaskServiceCamunda extends BpmsTaskService {
    @Value("${recpro.bpm.api.url}")
    public  String BASE_URL;

    @Override
    public void claim(String taskId, String assigneeId) {
        RequestHandler.sendRequest(BASE_URL + TASK_CLAIM + taskId + "?userId=" + assigneeId);
    }

    @Override
    public void complete(String taskId) {
        RequestHandler.sendRequest(BASE_URL + TASK_COMPLETE +taskId);
    }

    @Override
    public void unclaim(String taskId) {
        RequestHandler.sendRequest(BASE_URL + TASK_UNCLAIM + taskId);
    }

    @Override
    public void setAttribute(String attributeId, String taskId, Object value) {
        RequestHandler.sendRequest(BASE_URL + SET_TASK_ATTRIBUTE + taskId + "?" + REQUEST_PARAM_ATTRIBUTE_ID + attributeId + REQUEST_PARAM_ATTRIBUTE_VALUE + value);
    }

    @Override
    public void setAttributes(String taskId, Map<String, Object> attributes) {
        RequestHandler.sendRequest(BASE_URL + SET_TASK_ATTRIBUTES + taskId, attributes);
    }

}
