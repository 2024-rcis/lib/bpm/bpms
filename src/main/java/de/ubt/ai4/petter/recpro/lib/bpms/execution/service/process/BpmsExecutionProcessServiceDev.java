package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.process;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("dev-bpms")
@Service
public class BpmsExecutionProcessServiceDev extends BpmsExecutionProcessService{
}
