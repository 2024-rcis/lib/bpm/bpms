package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.task;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@AllArgsConstructor
public abstract class BpmsTaskService {

    public BpmsTask getById(String taskId) {
        return new BpmsTask();
    }

    public void claim(String taskId, String assigneeId) {

    }

    public void start(String taskId) {

    }

    public void complete(String taskId) {

    }

    public void unclaim(String taskId) {

    }

    public void cancel(String taskId) {

    }

    public void setAttribute(String attributeId, String taskId, Object value) {}

    public void setAttributes(String taskId, Map<String, Object> attributes) {}
}
