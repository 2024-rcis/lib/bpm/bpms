package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTasklist;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public abstract class BpmsTasklistService {
    public BpmsTasklist getTasklistByAssignee(String assigneeId) {
        return new BpmsTasklist();
    }

    public BpmsTasklist getTasklist(String assigneeId) {
        return new BpmsTasklist();
    }
}
