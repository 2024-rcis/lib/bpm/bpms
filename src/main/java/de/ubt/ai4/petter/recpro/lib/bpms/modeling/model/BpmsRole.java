package de.ubt.ai4.petter.recpro.lib.bpms.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsRole {
    private String id;
    private String name;
    private List<BpmsRole> childElements = new ArrayList<>();
}
