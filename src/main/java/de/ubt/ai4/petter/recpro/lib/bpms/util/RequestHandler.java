package de.ubt.ai4.petter.recpro.lib.bpms.util;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public abstract class RequestHandler {
    public static void sendRequest(String uri) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
        RestTemplate template = new RestTemplate();
        ResponseEntity<Void> response = template.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                null,
                Void.class
        );
        if (response.getStatusCode().isError()) {
            System.err.println("ERROR SENDING REQUEST TO CAMUNDA");
            System.err.println(builder.toUriString());
        }
    }

    public static void sendRequest(String uri, Object entity) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Object> requestEntity = new HttpEntity<>(entity, headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
        RestTemplate template = new RestTemplate();
        ResponseEntity<Void> response = template.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                requestEntity,
                Void.class
        );
        if (response.getStatusCode().isError()) {
            System.err.println("ERROR SENDING REQUEST TO CAMUNDA");
            System.err.println(builder.toUriString());
        }
    }
}
