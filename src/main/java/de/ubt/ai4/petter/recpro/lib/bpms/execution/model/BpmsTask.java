package de.ubt.ai4.petter.recpro.lib.bpms.execution.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsTask {

    private Long id;
    private String taskId;
    private String assignee;
    private String activityId;
    private String processInstanceId;
    private String processDefinitionId;
    private int priority;
    private Instant createDate;
    private Instant dueDate;
    private String executionId;
}
