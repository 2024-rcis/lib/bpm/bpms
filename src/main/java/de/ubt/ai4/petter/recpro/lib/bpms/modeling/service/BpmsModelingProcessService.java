package de.ubt.ai4.petter.recpro.lib.bpms.modeling.service;

import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsProcess;
import de.ubt.ai4.petter.recpro.lib.bpms.util.BpmApiUrls;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class BpmsModelingProcessService {


    @Value("${recpro.bpm.api.url}")
    public  String BASE_URL;
    public List<BpmsProcess> getAll() {
        RestTemplate restTemplate = new RestTemplate();
        return Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(BASE_URL + BpmApiUrls.PROCESS_GET_ALL, BpmsProcess[].class)));
    }

    public BpmsProcess getById(String processId) {
        return this.getAll().stream().filter(process -> process.getId().equals(processId)).findFirst().orElse(new BpmsProcess());
    }

}
