package de.ubt.ai4.petter.recpro.lib.bpms.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsProcess {
    private String id;
    private String name;
    private String description;
    private String version;
    private String key;
    private boolean latest;
    private BpmsProcessModel processModel;
}
