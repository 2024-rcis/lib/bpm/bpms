package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.process;

import de.ubt.ai4.petter.recpro.lib.bpms.util.RequestHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;

import static de.ubt.ai4.petter.recpro.lib.bpms.util.BpmApiUrls.*;

@Profile("camunda")
@Service
public class BpmsExecutionProcessServiceCamunda extends BpmsExecutionProcessService{

    @Value("${recpro.bpm.api.url}")
    public  String BASE_URL;

    @Override
    public void startProcess(String processId) {
        RequestHandler.sendRequest(BASE_URL + PROCESS_START + processId);
    }

    @Override
    public void setAttribute(String attributeId, String processInstanceId, Object value) {
        RequestHandler.sendRequest(BASE_URL + SET_PROCESS_ATTRIBUTE + processInstanceId + "?" + REQUEST_PARAM_ATTRIBUTE_ID + attributeId + REQUEST_PARAM_ATTRIBUTE_VALUE + value);
    }

    @Override
    public void setAttributes(String processInstanceId, Map<String, Object> attributes) {
        RequestHandler.sendRequest(BASE_URL + SET_PROCESS_ATTRIBUTES + processInstanceId, attributes);
    }

}
