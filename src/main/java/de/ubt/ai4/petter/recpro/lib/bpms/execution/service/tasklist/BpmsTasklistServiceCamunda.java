package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTask;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTasklist;
import de.ubt.ai4.petter.recpro.lib.bpms.util.BpmApiParameters;
import de.ubt.ai4.petter.recpro.lib.bpms.util.BpmApiUrls;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Profile("camunda")
@Service
public class BpmsTasklistServiceCamunda extends BpmsTasklistService {
    @Value("${recpro.bpm.api.url}")
    public  String BASE_URL;
    @Override
    public BpmsTasklist getTasklistByAssignee(String userId) {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + BpmApiUrls.EXECUTION_TASKLIST_GET_BY_ASSIGNEE_URL).queryParam(BpmApiParameters.ASSIGNEE_ID, userId);
        List<BpmsTask> tasks = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(builder.toUriString(), BpmsTask[].class)));
        return new BpmsTasklist(-1L, userId, tasks);
    }

    @Override
    public BpmsTasklist getTasklist(String userId) {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + BpmApiUrls.EXECUTION_TASKLIST_GET_ALL);
        List<BpmsTask> tasks = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(builder.toUriString(), BpmsTask[].class)));
        return new BpmsTasklist(-1L, userId, tasks);
    }
}
